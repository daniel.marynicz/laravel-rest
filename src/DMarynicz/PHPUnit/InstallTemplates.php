<?php
namespace DMarynicz\PHPUnit;

use Composer\Script\Event;
use Composer\Composer;
use DirectoryIterator;

/**
 * Install PHPUnit Templates
 *
 * @author Daniel Marynicz <daniel.marynicz at gmail dot com>
 */
class InstallTemplates
{
    /**
     *
     * @var Composer
     */
    protected $composer;

    /**
     *
     * @param Composer $composer
     */
    public function __construct(Composer $composer)
    {
        $this->composer = $composer;
    }

    /**
     * Installs PHPUnit templates
     *
     * @param Event $event
     */
    public static function install(Event $event)
    {
        $installer = new static($event->getComposer());
        $installer->installTemplatesIfPHPUnitInstalled();
    }

    /**
     * Installs PHPUnit templates
     */
    public function installTemplatesIfPHPUnitInstalled()
    {
        if (!$this->isPHPUnitInstalled()) {
            return;
        }
        foreach ($this->getMyTemplatesDirIterator() as $file) {
            if ($file->isFile()) {
                copy(
                    $file->getPathname(),
                    $this->getPHPUnitTemplatesDir(). DIRECTORY_SEPARATOR . $file->getFilename()
                );
            }
        }
    }

    /**
     * Returns templates dir
     *
     * @return string
     */
    public function getMyTemplatesDir()
    {
        return __DIR__.'/Template';
    }

    /**
     * Getter Composer
     *
     * @return Composer
     */
    public function getComposer()
    {
        return $this->composer;
    }

    /**
     * Returns DirectoryIterator for templates dir
     *
     * @return DirectoryIterator
     */
    protected function getMyTemplatesDirIterator()
    {
        return new DirectoryIterator($this->getMyTemplatesDir());
    }

    /**
     * Is PHPUnit Installed?
     *
     * @return boolean
     */
    private function isPHPUnitInstalled()
    {
        return is_dir($this->getPHPUnitTemplatesDir());
    }

    /**
     * Get full path to PHPUnit Dir
     *
     * @return string
     */
    private function getPHPUnitTemplatesDir()
    {
        return $this->getComposer()->getConfig()->get('vendor-dir').'/phpunit/phpunit-skeleton-generator/src/template';
    }
}
