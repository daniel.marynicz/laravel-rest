<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get(
    '/',
    function () {
        return view('welcome');
    }
);

Route::get('/'.config('api_path').'/storage/{path}', 'Api\StorageController@show');
Route::put('/'.config('api_path').'/storage/{path}', 'Api\StorageController@store');
Route::post('/'.config('api_path').'/storage/{path}', 'Api\StorageController@store');
Route::delete('/'.config('api_path').'/storage/{path}', 'Api\StorageController@destroy');
