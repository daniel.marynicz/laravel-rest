<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Simple Storage Controller
 *
 * @author Daniel Marynicz <daniel.marynicz at gmail dot com>
 */
class StorageController extends ApiController
{
    protected $storage;

    /**
     * Construct with dependency injection of Filesystem storage
     *
     * @param Filesystem $storage
     */
    public function __construct(Filesystem $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Store a new resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param string $file
     *
     * @return array
     */
    public function store(Request $request, $file)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'body' => 'required',
            ]
        );

        if ($validator->fails()) {
            throw new BadRequestHttpException(
                $this->getValidatorErrorsAsSring($validator->errors())
            );
        }

        $body = $request->input('body');

        return
        [
                'saved' => $this->storage->put($file, $body)
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param   string  $file
     *
     * @return array
     */
    public function show($file)
    {
        if (!$this->storage->exists($file)) {
            throw new NotFoundHttpException('Requested file not found');
        }

        return
        [
                'body' => $this->storage->get($file)
        ];


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param   string  $file
     *
     * @return array
     */
    public function destroy($file)
    {
        if (!$this->storage->exists($file)) {
            throw new NotFoundHttpException('Requested file not found');
        }

        return [
            'deleted' => $this->storage->delete($file)
        ];
    }
}
