<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;

/**
 * Description of ApiController
 *
 * @author Daniel Marynicz <daniel.marynicz at gmail dot com>
 */
class ApiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function callAction($method, $parameters)
    {
        $result =  call_user_func_array([$this, $method], $parameters);
        $result['error'] = false;

        return $result;
    }

    /**
     * Returns validator errors as string
     *
     * @param MessageBag $errors
     *
     * @return string
     */
    protected function getValidatorErrorsAsSring(MessageBag $errors)
    {
        $messages = '';
        foreach ($errors->all(":message\n") as $message) {
            $messages .= $message;
        }

        return $messages;
    }
}
