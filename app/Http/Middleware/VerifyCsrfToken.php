<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use \Illuminate\Contracts\Encryption\Encrypter;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [];

    public function __construct(Encrypter $encrypter)
    {
        $this->except[] = config('api_path').'/*';

        parent::__construct($encrypter);
    }
}
