<?php

namespace App\Traits;

use Illuminate\Filesystem\Filesystem;

/**
 * trait for creating, deleting temporary directories
 *
 * @author Daniel Marynicz <daniel.marynicz at gmail dot com>
 */
trait TemporaryDirectory
{
    /**
     * Temporary directory path
     *
     * @var string
     */
    protected $temporaryDirectory;

    /**
     * Getter for Temporary Directory
     * Create temp. dir if the directory does not exist.
     *
     * @return string path to temp. dir
     */
    protected function getTemporaryDirectory()
    {
        if ($this->temporaryDirectory) {
            return $this->temporaryDirectory;
        }

        $this->temporaryDirectory = $this->createTemporaryDirectory();

        return $this->temporaryDirectory;
    }

    /**
     * Create Temporary Directory
     *
     * @return string path to dir
     */
    protected function createTemporaryDirectory()
    {

        $tempFile = tempnam(sys_get_temp_dir(), '');
        unlink($tempFile);

        (new Filesystem)->makeDirectory($tempFile);

        return $tempFile;
    }

    /**
     * Recursively delete a temporary directory
     */
    protected function deleteTemporaryDirectory()
    {
        if ($this->temporaryDirectory && is_dir($this->temporaryDirectory)) {
            (new Filesystem)->deleteDirectory($this->temporaryDirectory);
        }
    }
}
