<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Debug\Exception\FlattenException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        if ($request->is(config('api_path').'/*')) {
            if (!$this->isHttpException($e)) {
                $e = $this->flattenException($e);
            }

            return response()->json(
                [
                    'error'   => true,
                    'message' => $e->getMessage()
                ],
                $e->getStatusCode()
            );
        }

        return parent::render($request, $e);
    }

    /**
     * FlattenException
     *
     * @param Exception $exception
     *
     * @return FlattenException
     */
    protected function flattenException(Exception $exception)
    {
        if (!$exception instanceof FlattenException) {
            $exception = FlattenException::create($exception);
        }

        if (!config('app.debug')) {
            $exception->setMessage(Response::$statusTexts[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }

        return $exception;
    }
}
